let http = require("http");

// Mock database
let directory = [
  {
    name: "Rendon",
    email: "Rendon@gmail.com",
  },
  {
    name: "Jobert",
    email: "jobert@email.com",
  },
  {
    name: "CongTV",
    email: "CongTV@email.com",
  },
  {
    name: "PINGRIS",
    email: "PINGRIS@email.com",
  },
];

http
  .createServer(function (request, response) {
    if (request.url == "/" && request.method == "GET") {
      response.writeHead(200, { "Content-Type": "text/plain" });

      // JSON.stringify() - Transform a javascript into JSON string
      response.write("Welcome to Booking System");
      response.end();
    }
    if (request.url == "/profile" && request.method == "GET") {
      response.writeHead(200, { "Content-Type": "text/plain" });

      // JSON.stringify() - Transform a javascript into JSON string
      response.write("Welcome to your profile!");
      response.end();
    }
    if (request.url == "/courses" && request.method == "GET") {
      response.writeHead(200, { "Content-Type": "text/plain" });

      // JSON.stringify() - Transform a javascript into JSON string
      response.write("Here are the available courses");
      response.end();
    }
    if (request.url == "/addcourse" && request.method == "PUT") {
      response.writeHead(200, { "Content-Type": "text/plain" });

      // JSON.stringify() - Transform a javascript into JSON string
      response.write("Add a course to our resources");
      response.end();
    }
    if (request.url == "/updatecourse" && request.method == "POST") {
      response.writeHead(200, { "Content-Type": "text/plain" });

      // JSON.stringify() - Transform a javascript into JSON string
      response.write("Update a course to our resources");
      response.end();
    }
    if (request.url == "/archivecourses" && request.method == "POST") {
      response.writeHead(200, { "Content-Type": "text/plain" });

      // JSON.stringify() - Transform a javascript into JSON string
      response.write("Archive courses to our resources");
      response.end();
    }
  })
  .listen(4000);

console.log("Server running at localhost:4000");
