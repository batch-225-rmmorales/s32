let http = require("http");

// Mock database
let directory = [
  {
    name: "Rendon",
    email: "Rendon@gmail.com",
  },
  {
    name: "Jobert",
    email: "jobert@email.com",
  },
  {
    name: "CongTV",
    email: "CongTV@email.com",
  },
  {
    name: "PINGRIS",
    email: "PINGRIS@email.com",
  },
];

http
  .createServer(function (request, response) {
    if (request.url == "/users" && request.method == "GET") {
      response.writeHead(200, { "Content-Type": "application/json" });

      // JSON.stringify() - Transform a javascript into JSON string
      response.write(JSON.stringify(directory));
      response.end();
    }

    // Route for creating a new item upon recieving a POST request
    // A request object contains several parts:
    // Headers - contains information about the request context/content like what is the data type.
    // Body - contain the actual information being sent with the request.
    // Note : The request body can NOT be empty
    else if (request.url == "/users" && request.method == "POST") {
      // Declare and initialize a "requestBody" variable to an empty string
      // This will act as a placeholder for the resource/data to be created later on.

      let requestBody = "";

      // A stream is sequence of data
      // Data is recieve from the client and is processed in the 'data' stream
      // The information provided from the request object enters a sequence called "data" the code below will be triggered
      // data step - this reads the "data" stream and processes it as the request body.

      // on method binds an event to a object
      // It is a way to express your intent if there is something happening (data sent or error in your case), then execute the function added as a parameter. This style of programming is called Event-driven programming
      // event-driven-programming is a programming paradigm in which the flow of the program is determined by event such as user actions(mouse clicks, key presses) or message passing from other programs or threads.

      request.on("data", function (data) {
        console.log(data.toString());
        // console.log(typeOf(data));
        requestBody += data;
      });

      request.on("end", function () {
        // Check if at this point the requestBody is of data type STRING
        // We need this to be of data type JSON to access its property.

        console.log(typeof requestBody);

        // JSON.parse() - Takes a JSON string and transform it into javascript objects.

        requestBody = JSON.parse(requestBody);

        // Create a new object representing the new mock database record

        let newUser = {
          name: requestBody.name,
          email: requestBody.email,
        };

        // Add the user into the mock database
        directory.push(newUser);
        console.log(directory);

        response.writeHead(201, { "Content-Type": "application/json" });
        response.write(JSON.stringify(newUser));
        response.end();
      });
    }
  })
  .listen(4000);

console.log("Server running at localhost:4000");
