//node js routing with http methods
let http = require("http");

const server = http
  .createServer((req, res) => {
    if (req.url == "/items" && req.method == "GET") {
      res.writeHead(200, { "Content-Type": "text/plain" });
      res.end("Data retrieved");
    }
    if (req.url == "/items" && req.method == "POST") {
      res.writeHead(200, { "Content-Type": "text/plain" });
      res.end("Data to be sent to the database");
    } else {
      res.writeHead(200, { "Content-Type": "text/plain" });
      res.end("404 not found");
    }
  })
  .listen(5000);

console.log("Server is running at localhost:4000");
